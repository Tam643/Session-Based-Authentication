# Session-Based Authentication

This is a solution to the [QR Code Component on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- Register in the project
- Use Authentication system (login and logout)
- Delete your account

### Screenshot

![](./picture/screencapture-localhost-3000-signin-2023-05-09-13_12_38.png)

### Links

- Live Site URL: [Live Site at Vercel](https://qr-code-component-f22bx2oqb-correlucas.vercel.app/)

## My process

### Built with

- ejs
- express
- express-session
- mysql2
- sequelize

### What I learned

- Use ejs work with express
- Build and config your server with express
- Manage the database with sequelize and mysql2
- Manage session with express-session

### Useful resources

- [ ejs ](https://shorturl.at/grtDU) - How To Use EJS to Template Your Node Application
- [ express ](https://expressjs.com/en/starter/hello-world.html) - Express Documentation
- [ Sequelize ](https://shorturl.at/dwEN6) - The Ultimate Guide To Get Started With Sequelize ORM
- [ express-session ](https://kokdev.com/memory/nodejs-session-cookie/) - การจัดการ Session Node.js ด้วย Express Session


## Author

- Github - [Tum](https://github.com/Tam643/Session-Based-Authentication)
